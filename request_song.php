<?php
   include("config.php");
   session_start();
   $MAX_AMOUNT_SONGREQUESTS = MAX_SONG_REQUESTS; //how many songs can a person add to the song queue?
   
   if(strlen($_POST["song"]) == 0 and strlen($_POST["artist"]) == 0){
	   header("location: logedin.php");
   } 
   else if($_SERVER["REQUEST_METHOD"] == "POST" and isset($_SESSION['login_user_id'])) {
      $song = mysqli_real_escape_string($db,$_POST['song']);
      $artist= mysqli_real_escape_string($db,$_POST['artist']);
	  $song_words = explode(" ",$_POST['song']);
	  $song_words_sql = "";
	  

	  
	  if(sizeof($song_words) == 0){
		  $song_words_sql = "song LIKE '%'";
	  }else{
		  $song_words_sql = "song LIKE";
		  foreach($song_words as $key => $word){
			  $song_words_sql = $song_words_sql . " '%" . $word . "%'";
			  if($key+1 < sizeof($song_words)){
				  $song_words_sql = $song_words_sql . " OR ";
			  }
		  }
	  }
	  
	  $sql = "SELECT * FROM availablesongs WHERE artist LIKE '%$artist%' AND $song_words_sql";
	  
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      //$active = $row['active'];
      
      $count = mysqli_num_rows($result);
	  
      if($count > 0) {
		  $filepath = $row['filepath'];
		  $artist = $row['artist'];
		  $song = $row['song'];
		  $youtubepath = $row['youtubepath'];
		  $uid = $_SESSION['login_user_id'];
		  print($sql);
		  print("\n");                                     
		  print(music_folder);
		  print($filepath);
		  $pass_arg = music_folder . $filepath;
		  
		  //add song to the request table!
		  
		  //check amount of requests currently in queue from user
		  $sql_user_current_requests = "SELECT COUNT(*) AS amount_of_requests FROM songrequests WHERE requester = $uid AND flag < 2";
		  $result2 = mysqli_query($db,$sql_user_current_requests);
		  $row2 = mysqli_fetch_array($result2,MYSQLI_ASSOC);
		  //check if song is already queued by the user
		  $sql_user_double_request = "SELECT COUNT(*) AS song_request_count FROM songrequests WHERE requester = $uid AND flag < 2 AND filepath LIKE '$filepath'";
		  $result3 = mysqli_query($db,$sql_user_double_request);
		  $row3 = mysqli_fetch_array($result3,MYSQLI_ASSOC);
		  
		  
		  if($row3['song_request_count'] > 0 ){//already requested this song?
			  print("<br>This song is already in the queue, requested by you!<br>");
			  $_SESSION['warning_message'] = "You have already requested this song...";
			  print($song . " " . $artist);
			  
		  }
		  elseif($row2['amount_of_requests'] >= $MAX_AMOUNT_SONGREQUESTS){ //already requested max amount of song requests?
			print("You have already requested $MAX_AMOUNT_SONGREQUESTS songs, which is the limit!");
			$new_msg = "You have already requested $MAX_AMOUNT_SONGREQUESTS songs, which is the limit!";
			$_SESSION['warning_message'] = $new_msg;
		  }else{ //add the song request to the database!
			$sql_add_request = "INSERT INTO songrequests (song,requester,filepath,weblink,artist) VALUES('$song',$uid,'$filepath','$youtubepath','$artist')";
			$result3 = mysqli_query($db,$sql_add_request);
			//$row3 = mysqli_fetch_array($result,MYSQLI_ASSOC);
		  }
      }else { //no search result for the song, need to download it?..
        $error = "Could not find song and/or artist in database.";
		$_SESSION['warning_message'] = "Could not match the database with this search, try again.";
		echo $error;
      }
	  header("location: loginhomepage.php");
   }else{
	   print("You are not logged in, cannot request a song...");
	   $_SESSION['warning_message'] = "Please login to request a song.";
   }
   //header("location: index.php");
$db->close();
?>