<?php
   include("config.php");
   session_start();
   
	if (isset($_SESSION['login_user'])){
		$uid = $_SESSION['login_user_id'];
		
		$sql_vote_check = "SELECT * FROM songrequests WHERE flag = 1 ORDER BY id DESC";
		$result = mysqli_query($db,$sql_vote_check);
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$idToSkip = $row['id'];
		$count_playing = mysqli_num_rows($result);

		$sql_check_vote = "SELECT * FROM uservotes WHERE user = $uid AND songrequest= $idToSkip";
		$result_check = mysqli_query($db,$sql_check_vote);
		$count = mysqli_num_rows($result_check);
		
		if($count_playing == 0){
			$_SESSION['warning_message'] = "There is no song currently playing, please request a song.";
		}
		elseif($count == 0){
			$sql_new_vote = "INSERT INTO uservotes (user,songrequest) VALUES ($uid,$idToSkip)";
			$result_vote = mysqli_query($db,$sql_new_vote);
			$_SESSION['success_msg'] = "You voted to skip the song that is currently playing.";
		}else{
			$_SESSION['warning_message'] = "You already voted to skip this song!";
		}
		header("location: logedin.php");
	}
	$db->close();
?>